package com.dslearn.entities;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter@Setter
@NoArgsConstructor
@Entity
@Table(name = "tb_task")
public class Task extends Lesson{
	private static final long serialVersionUID = 1L;
	
	private String description;
	private Integer questionCount;
	private Integer aprprovalCount;
	private Double weight;
	@Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
	private Instant dueDate;
	
	
	public Task(Long id, String title, Integer position, Section section, String description, Integer questionCount,
			Integer aprprovalCount, Double weight, Instant dueDate) {
		super(id, title, position, section);
		this.description = description;
		this.questionCount = questionCount;
		this.aprprovalCount = aprprovalCount;
		this.weight = weight;
		this.dueDate = dueDate;
	}
	

}
