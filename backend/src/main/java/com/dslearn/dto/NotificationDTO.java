package com.dslearn.dto;

import java.io.Serializable;
import java.time.Instant;

import com.dslearn.entities.Notification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NotificationDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String text;
	private Instant moment;
	private Boolean read;
	private String route;
	private Long userId;
	
	public NotificationDTO(Notification entity) {
		id = entity.getId();
		text = entity.getText();
		moment = entity.getMoment();
		read = entity.getRead();
		route = entity.getRoute();
		userId = entity.getUser().getId();
	}
	
	
}
