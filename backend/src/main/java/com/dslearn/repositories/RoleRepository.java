package com.dslearn.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dslearn.entities.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
