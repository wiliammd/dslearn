package com.dslearn.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.dslearn.entities.Notification;
import com.dslearn.entities.User;

public interface NotificationRepository extends JpaRepository<Notification, Long> {

	@Query("SELECT obj FROM Notification obj WHERE "
			+ "(obj.user = :user) AND"
			+ "(:unreadOnly = false OR obj.read = :unreadOnly) "
			+ "ORDER BY obj.moment DESC")
	Page<Notification> find(User user,boolean unreadOnly, Pageable pageable);
	//mesma consulta que a de cima
	//Page<Notification> findByUserAndReadOrderByMomentDesc(User user,boolean unreadOnly, Pageable pageable);
}
