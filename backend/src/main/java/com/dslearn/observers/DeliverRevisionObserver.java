package com.dslearn.observers;

import com.dslearn.entities.Deliver;

public interface DeliverRevisionObserver {
	void onSaveRevision(Deliver deliver);
}
